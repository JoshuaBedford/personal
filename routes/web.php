<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/photography', 'PhotographyController@index');
Route::get('/photography/{id}', 'PhotographyController@show');

Route::get('/projects', 'ProjectsController@index');
Route::get('/projects/{type}', 'ProjectsController@index');
Route::get('/projects/{type}/{slug}', 'ProjectsController@show');

Route::get('/blog', 'BlogController@index');
Route::get('/blog/{slug}', 'BlogController@show');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/{page}', 'PagesController@show');