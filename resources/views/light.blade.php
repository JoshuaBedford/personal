<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Joshua Bedford</title>
        
        <!-- <link rel="stylesheet" href="{{ asset('css/all.css') }}"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">

        <!-- Font Awesome -->
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,600" rel="stylesheet" type="text/css">

        <style>
            hr {
                margin-top: 0;
                margin-bottom: 0;
                border-top: 1px solid #999;
            }
        </style>

        <!-- @yield('css.local') -->
        <style type="text/css">
            body{
                margin: 0;
                background: #000;
            }
            .is-centered{
                align-items: center;
                justify-content: center;
            }
            .grayscale > a.button{
                color: #999;
                text-transform: uppercase;
                padding: 10px;
                border: 1px solid #999;
                height: 25px;
                /*width: 100%;*/
                font-size: 1.2em;
                text-align: center;
            }
            img{
                width: 100%;
                /*max-height: 100vh;*/
            }
            .grayscale > img {
              -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
              filter: grayscale(100%);
              opacity: 0.7;
            }
            .column{
                padding: 0;
            }
            .columns{
                margin: 0;
                padding: 0;
            }
            .preview{
                max-height: 100vh;
                overflow: hidden;
            }
        </style>

    </head>
    <body>
<!--         <div class="flex-center position-ref full-height">

            <div class="column-3 grayscale">
                <img src="{{ asset('/images/development.jpg') }}">
                <a class="button">
                    Development
                </a>
            </div>
            <div class="column-3 grayscale">
                <img src="{{ asset('/images/photography.jpg') }}">
                <a class="button">
                    Photography
                </a>
            </div>
            <div class="column-3 grayscale">
                <img src="{{ asset('/images/woodworking.jpg') }}">
                <a class="button">
                    Woodworking
                </a>
            </div>
            <div class="column-3 grayscale">
                <img src="{{ asset('/images/knowledgebase.jpg') }}">
                <a class="button">
                    Knowledgebase
                </a>
            </div>
        </div> -->

        <div class="columns preview">
            <div class="column grayscale">
                <img src="{{ asset('/images/development.jpg') }}">
                <a class="button">
                    Development
                </a>
            </div>
            <div class="column grayscale">
                <img src="{{ asset('/images/photography.jpg') }}">
                <a class="button">
                    Photography
                </a>
            </div>
            <div class="column grayscale">
                <img src="{{ asset('/images/woodworking.jpg') }}">
                <a class="button">
                    Woodworking
                </a>
            </div>
            <div class="column grayscale">
                <img src="{{ asset('/images/knowledgebase.jpg') }}">
                <a class="button">
                    Knowledgebase
                </a>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

        <!-- Green Sock CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                var grayscale = $(".grayscale");

                grayscale.hover(function(){
                    TweenMax.to($(this).find("img"), 0.8, {
                        css: {
                            'filter': 'grayscale(0%)',
                            opacity: "1",
                        }
                    });

                    TweenMax.to($(this).find(".button"), 0.8, {
                        css: {
                            'background': '#fff',
                            'color': '#000',
                            'border-color': '#000'
                        }
                    });
                }, function(){
                    TweenMax.to($(this).find("img"), 0.8, {
                        css: {
                            'filter': 'grayscale(100%)',
                            opacity: "0.8"
                        }

                    });

                    TweenMax.to($(this).find(".button"), 0.8, {
                        css: {
                            'background': 'none',
                            'color': '#fff',
                            'border-color': '#fff'
                        }
                    });
                });
                
            });
        </script>
    </body>
</html>
