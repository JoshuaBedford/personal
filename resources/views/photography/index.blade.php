@extends('layouts.main')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="margin--bottom-30">
        <span>
            Order By: &nbsp;
            @if($order == 'popular')
                <span class="light-gray-text">
                    Popular
                </span>
            @else
                <a class="light-gray-text" href="{{ url('/photography?order=popular') }}">Popular</a>
            @endif
            &nbsp; | &nbsp;
            @if($order == 'latest')
                <span class="light-gray-text">
                    Latest
                </span>
            @else
                <a class="light-gray-text" href="{{ url('/photography?order=latest') }}">Latest</a>
            @endif
        </span>
  {{--       <span style="float: right">
            Photos available on 
            <img style="margin-top: -5px; height: 15px;" src="{{ url('/images/Unsplash_Logo_Full.svg') }}" alt="unsplash logo">
        </span> --}}
    </div>
    <div class="row">

        @foreach($photos as $photo)
            <div class="col-md-6">
                {{-- <a href="{{ url('/photography/' . $photo->id) }}"> --}}
                    <div class="panel panel-primary">
                        <div class="panel-image">
                            <img src="{{ $photo->urls['regular'] }}" alt="">
                        </div>
                        <div class="panel-footer text-center dark-gray-background light-gray-text">
                            <span style="float: left;">
                                {{ number_format($photo->statistics()['views']->total) }}
                            </span>
                            <span style="float: right">
                                <a class="light-gray-text no-underline icon icon--light-gray icon--2x" href="{{ url('/photography/' . $photo->id) }}" alt="View image on Unsplash" title="View image on Unsplash">
                                    {{-- <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="eye" class="svg-inline--fa fa-eye fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"></path></svg> --}}
                                    View
                                </a>
                            </span>
                        </div>
                    </div>
                {{-- </a> --}}
            </div>
        @endforeach

    </div>
    @if($page >= 1)
    <div style="height: 3em" class="margin--bottom-30">
        @if($page > 1)
        <div style="float: left;">
            <a class="light-gray-text" href="{{ url('/photography?page=' . ($page - 1)) }}">< previous</a>
        </div>
        @endif
        @if($page < $count['pageCount'])
        <div style="float: right;">
            <a class="light-gray-text" href="{{ url('/photography?page=' . ($page + 1)) }}">next ></a>
        </div>
        @endif
    </div>
    @endif
</div>
@stop