@extends('layouts.main')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="row">

        <div class="col-md-12">
            {{-- <a href="{{ $photo->links['html'] }}"> --}}
                <div class="panel panel-primary panel--auto-width panel--auto-center">
                    <div class="panel-image">
                        <img src="{{ $photo->urls['regular'] }}" alt="">
                    </div>
                        <div class="panel-footer text-center dark-gray-background">
                            <span style="float: left">
                                Views: {{ number_format($photo->statistics()['views']->total) }}
                            </span>
                            <span style="float: right">
                                <a class="light-gray-text no-underline margin--top-10 icon icon--light-gray icon--2x" href="{{ $photo->links['html'] }}" alt="View image on Unsplash" title="View image on Unsplash">
                                    View/Download On &nbsp;
                                    <img style="margin-top: -5px; height: 15px;" src="{{ url('/images/Unsplash_Logo_Full.svg') }}" alt="unsplash logo">
                                </a>
                            </span>
                        </div>
                </div>
            {{-- </a> --}}

            <p class="align-center padding--top-20">
                <a class="light-gray-text" href="{{ url('/photography') }}">back to photos</a>
            </p>
        </div>

    </div>
</div>
@stop