@extends('layouts.main')

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="row">

        @foreach($projects as $project)
            <div class="col-md-6">
                <a href="{{ url('/projects/' . $project->type . '/' . $project->slug) }}">
                    <div class="panel panel-primary">
                        <div class="panel-image">
                            <img src="{{ url('/storage/' . $project->featured_image) }}" alt="">
                        </div>
                        <div class="panel-footer text-center dark-gray-background light-gray-text pad-30">
                            {{ $project->title }}
                        </div>
                    </div>
                </a>
            </div>
        @endforeach

    </div>
</div>
@stop

