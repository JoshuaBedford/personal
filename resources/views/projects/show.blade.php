@extends('layouts.main')

@section('meta')
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="{{ $project->title }}" />
	<meta property="og:description" content="{{ $project->excerpt }}" />
	<meta property="og:image" content="{{ url('/storage/' . $project->featured_image) }}" />
@stop

@section('title')
{{ $project->title }}
@stop

@section('content')
<div class="col-md-10 col-md-offset-1 project">
    <div class="content">
        <h1 class="title">{{ $project->title }}</h1>
        <div class="excerpt">
            {!! $project->excerpt !!}
        </div>
        {!! $project->content !!}
    </div>
</div>
@stop