@extends('layouts.main')

@section('custom-css')
<style>
    .panel > .panel-header{
        min-height: 64px;
        margin: auto;
    }
    .panel > .panel-body{
        min-height: 100px;
    }
    .panel > .panel-footer{
        background-color: #fff;
    }
</style>
@stop

@section('content')
<div class="col-md-10 col-md-offset-1 content blog">
    <h1 class="title">
        Articles
    </h1>
    @foreach($articles as $article)
        <div class="col-md-8 col-md-offset-2 article">

            <h2>
                <a href="{{ url('/blog/' . $article->slug) }}" style="text-decoration: none;">
                {{ $article->title }}
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 25 25" xml:space="preserve">
                        <title>icon-arrow</title>
                        <path class="st0" d="M23.2,11.8l-0.7-0.7l-5-5c-0.4-0.4-1-0.5-1.4-0.1c-0.4,0.4-0.5,1-0.1,1.4c0,0,0.1,0.1,0.1,0.1l4,4H2.5  c-0.6,0-1,0.4-1,1s0.4,1,1,1h17.7l-4,4c-0.4,0.4-0.5,1-0.1,1.4c0.4,0.4,1,0.5,1.4,0.1c0,0,0.1-0.1,0.1-0.1l4.9-4.9l0.7-0.7  C23.7,12.9,23.7,12.3,23.2,11.8C23.3,11.8,23.3,11.8,23.2,11.8z"/>
                    </svg>
                </a>
            </h2>

            @include('includes/partials/_deprecated-tag')
            @include('includes/partials/_outdated-tag')

            <div class="excerpt">
                {{ $article->excerpt }}
            </div>
        </div>
    @endforeach
</div>
@stop