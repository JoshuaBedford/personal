@extends('layouts.main')

@section('custom-css')
@stop

@section('meta')
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="{{ $article->title }}" />
	<meta property="og:description" content="{{ $article->excerpt }}" />
	<meta property="og:image" content="{{ url('/storage/' . $article->featured_image) }}" />
@stop

@section('title')
{{ $article->title }}
@stop

@section('content')
<div class="col-md-10 col-md-offset-1 article">
	<div class="col-md-10 col-md-offset-1">
		<h1 class="title">
			{{ $article->title }}
		</h1>
		
		@include('includes/partials/_deprecated-tag')
		@include('includes/partials/_outdated-tag')

	</div>
	<div class="col-md-10 col-md-offset-1 body white-text">
		{!! $article->body !!}
	</div>
</div>
@stop