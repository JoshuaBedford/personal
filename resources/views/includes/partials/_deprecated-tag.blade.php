@if($article->deprecated == 1)
<div class="tag danger"
  title="This article is no longer supported. Most likely because technology or time has changed the accuracy or my opinion of the subject matter.">
    deprecated <u>(?)</u>
</div>
@endif