@if($article->outdated == 1)
<div class="tag caution"
  title="This article has not been verified recently. Information could be outdated.">
    outdated <u>(?)</u>
</div>
@endif