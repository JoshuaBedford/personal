<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Joshua Bedford</title>
        
        <link rel="stylesheet" href="{{ asset('css/all.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div id="flex" class="flex-center position-ref full-height">

            <div id="content" class="intro">
                <div class="title intro">
                    Joshua Bedford
                </div>

                <div class="typed">
                    <span class="element"></span>
                    <span class="final hideElement"></span><br>
                </div>
                    <span class="skip" style="margin-top: 4em; color: #939393;"><a onclick="skip()" style="color: #484848; cursor: pointer;">skip intro</a></span>

                <div class="body hideElement">

                    <div class="links dark">
                        <a href="{{ url('/about') }}">About</a>
                        <a href="{{ url('/web') }}">Web</a>
                        <a href="{{ url('/photo') }}">Photography</a>
                        <a href="{{ url('/knowledgebase') }}">KnowledgeBase</a>
                        <hr>
                    </div>


                    <div class="col-md-8 col-md-offset-2">
                        <p>
                            Welcome to my personal site! My name is Joshua Bedford and I am a <?php echo date_diff(date_create(date("Y-m-d")), date_create('1995-10-15'))->format("%Y"); ?> year old developer living in Mississippi (United States). A few of my projects include the <a href="{{ ('http://ergon.com') }}" target="_blank">Ergon, Inc.</a> intranet, <a href="{{ url('http://thelightmedia.net') }}" target="_blank">TheLightMedia.net</a>, <a href="{{ url('http://1LightforGod.net') }}" target="_blank">1LightforGod.net</a>, and <a href="{{ url('http://PeterChristianAndCrew.com') }}" target="_blank">PeterChristianAndCrew.com</a>, and many of my web projects can be found in the 'web' section.
                        </p>
                        <p>
                            But Web Development is not the only thing I do. I enjoy landscape and architectural photography, and occasionally people as well. My photos can be found on my photography page of this website, including my trips to Pigeon Forge, TN, Washington, D.C., the MS State Fair, and New Orleans, LA. Aside from photography, I also enjoy writing on occasion.
                        </p>
                        <p>
                            I've had the pleasure of meeting many different authors, and writing on many different blogs, but a large majority of my writings can be found on <a href="{{ url('http://1LightforGod.net') }}" target="_blank">1LightforGod.net</a>, in my book <a href="{{ url('https://www.amazon.com/weSHINE-Foundations-Strong-Walk-God-ebook/dp/B00EOBPPHE/ref=sr_1_1?ie=UTF8&qid=1500914742&sr=8-1&keywords=weshine+joshua') }}">weShine: Foundations of a Strong Walk With God</a>, or <a href="{{ url('/blog') }}">here</a> on this website.
                        </p>
                    </div>
            
                    <div class="col-md-12">
                        <hr>
                        @foreach($articles as $article)
                        <div class="col-md-6" style="min-height: 150px;">
                            <div class="panel panel-primary">
                                <div class="panel-header text-center blue-background white-text">
                                    {{ $article->title }}
                                </div>
                                <div class="panel-body">
                                    <p>
                                        {!! $article->excerpt !!}
                                    </p>
                                    <p>
                                        <span class="pull-right">
                                            <a href="{{ url('/knowledgebase/' . $article->slug) }}" style="text-decoration: none;">
                                                READ MORE
                                                <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="{{ url('js/typed.min.js') }}"></script>
        <script>

            var skipped = false;

            function skip(){
                skipped = true;
                revealContent();
            }

            function revealContent(){
                $(".skip").toggle();
                $(".hideElement").fadeToggle(2500);
                $(".element").replaceWith("Christ Follower. Website Developer. Landscape Photographer.").fadeIn('slow');
                $("#content").removeClass("intro");
                
                $("#content").fadeIn(2500, function(){
                    $("#content").addClass("content");
                });
            }

            $(function(){
                $(".element").typed({
                    strings: ["I am a...^1000", "Christ Follower.^1000 Website Developer.^1000 Landscape Photographer.^1000"],
                    typeSpeed: 0,
                    backSpeed: 5,
                    backDelay: 1000,
                    callback: function(){
                        if(!skipped){
                            revealContent();
                        }
                    }
                });
            });
        </script>
    </body>
</html>
