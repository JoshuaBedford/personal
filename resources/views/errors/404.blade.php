<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="Joshua Bedford resides in the Jackson, MS area and specializes in web design and development as well as woodworking and photography."/>

        <title>Joshua Bedford</title>
        
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,600" rel="stylesheet" type="text/css">

        <style>
            hr {
                margin-top: 0;
                margin-bottom: 0;
                border-top: 1px solid #999;
            }
        </style>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ setting('site.google_analytics_tracking_id') }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '{{ setting('site.google_analytics_tracking_id') }}');
        </script>

        @yield('css.local')

    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div id="content" class="intro">
                <div class="title intro">
                    You got 404'd.
                </div>

                <div class="typed">
                    <span class="element"></span>
                    <span class="final hideElement"></span><br>
                </div>

                <div class="body">

                    {{ menu('home', 'includes/nav/_home') }}
                    
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="{{ url('js/typed.min.js') }}"></script>
        <script>

            $(function(){
                $(".element").typed({
                    strings: ["{{ $exception->getMessage() ? $exception->getMessage() : 'Couldn\'t find that.^250 Try one of the links below.' }}"],
                    // strings: ["<span style='font-size: 1.5em'>Child of the Maker,^1000 a maker of many things.</span>^2000 </br /> Christ Follower ^1000| Developer ^1000| Photographer ^1000| Woodworker^1000"],
                    typeSpeed: 0,
                    backSpeed: 5,
                    backDelay: 1000
                });
            });
        </script>
    </body>
</html>
