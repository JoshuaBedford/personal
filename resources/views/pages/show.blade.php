@extends('layouts.main')

@section('meta')
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:description" content="{{ $page->excerpt }}" />
    <meta property="og:image" content="{{ url('/storage/' . $page->featured_image) }}" />
@stop

@section('title')
{{ $page->title }}
@stop

@section('custom-css')

    <style>
        ul.no-dots{
            list-style-type: none;
        }
    </style>

@stop

@section('content')
<div class="col-md-10 col-md-offset-1">
    <div class="col-md-8 col-md-offset-2 content" style="text-align: left;">
        
        <h1 class="title">
            {{ $page->title }}
        </h1>

        {!! $page->body !!}

    </div>
</div>
@stop
