@include('includes._header')

@include('includes._htmlheader')

<div class="body">
	@yield('content')
</div>

@include('includes._footer')