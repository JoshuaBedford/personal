<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Post as VoyagerPost;
use App\Scopes\ActiveScope;


class Post extends VoyagerPost
{
    // /**
    //  * The "booting" method of the model.
    //  *
    //  * @return void
    //  */
    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope(new ActiveScope);
    // }
}
