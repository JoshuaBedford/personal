<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \TCG\Voyager\Models\Page;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
    	$page = Page::whereStatus('ACTIVE')->whereSlug($slug)->first();

        if(!$page){
            abort(404);
        }

    	return view('pages.show', compact('page'));
    }
}
