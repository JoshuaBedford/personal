<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Photo;

use Unsplash\HttpClient as UnsplashAPI;
use Unsplash\User as UnsplashUser;
use Unsplash\Photo as UnsplashPhoto;

class PhotographyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        /*
         * Unsplash API Call
         * Get Photos by User
         * https://github.com/unsplash/unsplash-php#unsplashuserphotospage-per_page-order_by
         */

        $unsplashUser = UnsplashUser::find('joshuabedford');

        $order = $request->query('order');
        $page = $request->query('page') ? $request->query('page') : 1;
        $count = [
            'photoCount' => $unsplashUser->total_photos,
            'pageCount'  => ceil($unsplashUser->total_photos/10)
        ];

        $photos = $unsplashUser->photos($page, 10, $order);

        return view('photography.index', compact('photos', 'order', 'page', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photo = UnsplashPhoto::find($id);
        // dd($photo->statistics()['views']->total);
        return view('photography.show', compact('photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
