<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null)
    {
    	if(!$type){
    		$projects = Project::orderBy('order', 'ASC')->orderBy('created_at', 'DESC')->get();
    	} else{
    		$projects = Project::orderBy('order', 'ASC')->orderBy('created_at', 'DESC')->whereType($type)->get();

            if($projects->isEmpty()){
                abort(404, "Sorry! That project category doesn't exist. Try the nav below.");
            }
    	}


    	return view('projects.index', compact('projects', 'type'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($type, $slug)
    {
    	$project = Project::whereSlug($slug)->first();

        if(!$project){
            abort(404);
        }

    	return view('projects.show', compact('project'));
    }
}
