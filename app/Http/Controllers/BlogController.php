<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \TCG\Voyager\Models\Post;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Post::published()->orderBy('created_at', 'DESC')->get();

        return view('posts.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  string slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $article = Post::published()->whereSlug($slug)->first();

        if($article){
            return view('posts.show', compact('article'));
        } else{
            abort(404);
        }
    }
}
