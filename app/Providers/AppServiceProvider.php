<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Unsplash\HttpClient as UnsplashAPI;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
         * Initialize & Authorize Unsplash API
         */
        
        UnsplashAPI::init([
            'applicationId' => env('UNSPLASH_APP_ID'),
            'utmSource' => 'JoshuaBedford.com'
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
